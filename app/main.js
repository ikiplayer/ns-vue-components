import Vue from "nativescript-vue";
import App from "./components/App";
// import Login from "./components/Login/LoginType3";
import Home from "./components/Home/HomeType2";
import Test from "./pages/FoodApp/Home/Home.vue";
import HomeIndex from "./pages/Examples/Home/HomeIndex.vue";

import store from "./store";
import VueDevtools from "nativescript-vue-devtools";
import RadListView from "nativescript-ui-listview/vue";
// import VueI18n from "vue-i18n";
// import en from "./lang/en.default.js";

// PACKAGES

if (TNS_ENV !== "production") {
  Vue.use(VueDevtools);
}

Vue.use(RadListView);

Vue.registerElement(
  "LottieView",
  () => require("nativescript-lottie").LottieView
);

// Register element
Vue.registerElement(
  "CardView",
  () => require("@nstudio/nativescript-cardview").CardView
);

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = TNS_ENV === "production";

// Languages
// const messages = {
//   en: {
//     message: en
//   }
// };

// const i18n = new VueI18n({
//   locale: "en", // set locale
//   fallbackLocale: "en",
//   messages: messages // set locale messages
// });

new Vue({
  store,
  render: (h) => h("frame", [h(HomeIndex)]),
}).$start();
