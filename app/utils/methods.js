export function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export function checkNetwork() {
    const connectionType = connectivity.getConnectionType();
    switch (connectionType) {
        case connectivity.connectionType.none:
            alert("No network connection available!");
            return {
                id: 0, label: 'No network connection'
            };
        case connectivity.connectionType.wifi:
            alert("You are on wifi!");
            return {
                id: 1, label: "Wifi"
            };
        case connectivity.connectionType.mobile:
            alert("You are on a mobile data network!");
            return {
                id: 2, label: "Mobile Data"
            };
    }
}